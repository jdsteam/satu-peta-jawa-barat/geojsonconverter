FROM python:3.8-alpine
LABEL maintainer="pipinfitriadi@gmail.com"

COPY geojsonconverter/requirements.txt /

RUN apk add --no-cache --virtual .build-deps \
    && pip3 install --no-cache-dir -r /requirements.txt \
    && find /usr/local \
        \( -type d -a -name test -o -name tests \) \
        -o \( -type f -a -name '*.pyc' -o -name '*.pyo' \) \
        -exec rm -rf '{}' + \
    && runDeps="$( \
        scanelf --needed --nobanner --recursive /usr/local \
            | awk '{ gsub(/,/, "\nso:", $2); print "so:" $2 }' \
            | sort -u \
            | xargs -r apk info --installed \
            | sort -u \
        )" \
    && apk add --virtual .rundeps $runDeps \
    && apk del --no-cache .build-deps

COPY . /app
WORKDIR /app
RUN rm -f geojsonconverter/requirements.txt

EXPOSE 5000

# Gunicorn v 19 gives timeout for Flask http get request which taking time to execute:
# https://github.com/benoitc/gunicorn/issues/2020
CMD ["gunicorn", "-w", "4", "-t", "400", "-b", "0.0.0.0:5000", "geojsonconverter.wsgi:app"]
