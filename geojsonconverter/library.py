#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from geojson import (
    Feature,
    FeatureCollection,
    GeometryCollection,
    LineString,
    MultiLineString,
    MultiPoint,
    MultiPolygon,
    Point,
    Polygon
)


def json2geojson(dict_data, dict_params):
    '''
    >>> # In this helper, I try to cover every possible scenario case for use
    ... # this function.
    ...
    >>> dict_data = {
    ...     'random_params': None,
    ...     'latitude': 4.,
    ...     'longitude': 5.,
    ...     'coordinate': [
    ...         {
    ...             'x': '1',
    ...             'y': 2.,
    ...             'place': 'Bali'
    ...         },
    ...         {
    ...             'x': '1',
    ...             'y': '3',
    ...             'place': 'New Jersey'
    ...         },
    ...         {
    ...             'x': 1.,
    ...             'y': '4',
    ...             'place': 'Mecca'
    ...         }
    ...     ],
    ...     'line': [
    ...         [8.919, 44.4074],
    ...         [8.923, 44.4075]
    ...     ],
    ...     'location': [3, 4],
    ...     'shape': [
    ...         [
    ...             [2.38, 57.322],
    ...             [23.194, -20.28],
    ...             [-120.43, 19.15],
    ...             [2.38, 57.322]
    ...         ],
    ...         [
    ...             [-5.21, 23.51],
    ...             [15.21, -10.81],
    ...             [-20.51, 1.51],
    ...             [-5.21, 23.51]
    ...         ]
    ...     ],
    ...     'more_lines': [
    ...         [
    ...             [3.75, 9.25],
    ...             [-130.95, 1.52]
    ...         ],
    ...         [
    ...             [23.15, -34.25],
    ...             [-1.35, -4.65],
    ...             [3.45, 77.95]
    ...         ]
    ...     ],
    ...     'object_geometry': {
    ...         'more_points': [
    ...             {
    ...                 'data': {
    ...                     'coordinates': [
    ...                         [-155.52, 19.61],
    ...                         [-156.22, 20.74],
    ...                         [-157.97, 21.46]
    ...                     ],
    ...                     'type': 'MultiPoint'
    ...                 }
    ...             },
    ...             {
    ...                 'data': {
    ...                     'coordinates': [
    ...                         [-155.51, 19.62],
    ...                         [-156.21, 20.72],
    ...                         [-157.91, 21.42]
    ...                     ],
    ...                     'type': 'MultiPoint'
    ...                 }
    ...             }
    ...         ],
    ...         'more_polygons': [
    ...             [
    ...                 [
    ...                     [3.78, 9.28],
    ...                     [-130.91, 1.52],
    ...                     [35.12, 72.234],
    ...                     [3.78, 9.28]
    ...                 ],
    ...                 [
    ...                     [3.71, 9.29],
    ...                     [-130.90, 1.51],
    ...                     [35.11, 72.233],
    ...                     [3.77, 9.27]
    ...                 ]
    ...             ],
    ...             [
    ...                 [
    ...                     [23.18, -34.29],
    ...                     [-1.31, -4.61],
    ...                     [3.41, 77.91],
    ...                     [23.18, -34.29]
    ...                 ]
    ...             ]
    ...         ]
    ...     },
    ...     'this_is_points': [
    ...         [-155.52, 19.61],
    ...         [-156.22, 20.74],
    ...         [-157.97, 21.46]
    ...     ]
    ... }
    >>> dict_params = {
    ...     'geojson_latitude': 'latitude',
    ...     'geojson_longitude': 'longitude',
    ...     'coordinate': {
    ...         'geojson_longitude': 'x',
    ...         'geojson_latitude': 'y'
    ...     },
    ...     'geojson_linestring': 'line',
    ...     'geojson_point': 'location',
    ...     'geojson_polygon': 'shape',
    ...     'geojson_multilinestring': 'more_lines',
    ...     'geojson_multipoint': 'this_is_points',
    ...     'object_geometry': {
    ...         'more_points': {
    ...             'geojson_multipoint': 'data'
    ...         },
    ...         'geojson_multipolygon': 'more_polygons'
    ...     }
    ... }
    >>> from geojsonconverter.library import json2geojson
    >>> data = json2geojson(dict_data, dict_params)
    >>> from json import dumps
    >>> data = dumps(data, indent=4)
    >>> # I will not write the print output, because it will be too long for
    ... # display.
    ...
    >>> print(data)
    '''

    return_dict = {}

    if (
        dict_data
        and dict_params
        and isinstance(dict_params, dict)
    ):
        if isinstance(dict_data, dict):
            found_latitude_longitude = False
            return_geo = []
            geo_params = [
                'geojson_multipolygon',
                'geojson_multilinestring',
                'geojson_multipoint',
                'geojson_polygon',
                'geojson_linestring',
                'geojson_point',
                'geojson_latitude',
                'geojson_longitude'
            ]

            for geo_key in (
                dict_params_keys_set := set([
                    key.lower()
                    for key in dict_params.keys()
                ])
            ).intersection(
                geo_params_set := set(geo_params)
            ):
                if geo_key == 'geojson_multipolygon':
                    geo_func = MultiPolygon
                elif geo_key == 'geojson_multilinestring':
                    geo_func = MultiLineString
                elif geo_key == 'geojson_multipoint':
                    geo_func = MultiPoint
                elif geo_key == 'geojson_polygon':
                    geo_func = Polygon
                elif geo_key == 'geojson_linestring':
                    geo_func = LineString
                elif (
                    geo_key == 'geojson_point'
                    or (
                        (latitude := dict_params.get('geojson_latitude'))
                        and (longitude := dict_params.get('geojson_longitude'))
                        and not found_latitude_longitude
                    )
                ):
                    geo_func = Point

                    if geo_key != 'geojson_point':
                        geo_key = longitude, latitude
                        found_latitude_longitude = True
                else:
                    continue

                if (
                    not isinstance(geo_key, tuple)
                    and not (geo_key := dict_params.get(geo_key))
                ):
                    geo_func = None

                if geo_func:
                    if isinstance(geo_key, tuple):
                        geo_value = []

                        for coordinate_key in geo_key:
                            if isinstance(
                                value := dict_data.pop(coordinate_key, None),
                                str
                            ):
                                try:
                                    value = float(value)
                                except ValueError:
                                    value = None

                            geo_value.append(value)
                    else:
                        geo_value = dict_data.pop(geo_key, None)

                    if (
                        isinstance(geo_value, dict)
                        and (
                            (
                                geo_value_keys := set([
                                    key.lower()
                                    for key in geo_value.keys()
                                ])
                            ) == set(
                                ['coordinates', 'type']
                            )
                            or geo_value_keys == set(
                                ['geometry', 'properties', 'type']
                            )
                            or geo_value_keys == set(
                                ['features', 'type']
                            )
                        )
                    ):
                        geometry = geo_value
                    elif geo_value:
                        try:
                            geometry = geo_func(geo_value)
                        except ValueError:
                            geometry = None
                    else:
                        geometry = None

                    if geometry:
                        return_geo.append(
                            Feature(geometry=geometry)
                        )

            for key in (dict_params_keys_set - geo_params_set):
                if (
                    found_data := json2geojson(
                        dict_data.get(key),
                        dict_params.get(key)
                    )
                ):
                    dict_data[key] = found_data

            if return_geo:
                (first_return_geo := return_geo[0])['properties'] = dict_data

                if len(return_geo) == 1:
                    return_dict = first_return_geo
                else:
                    return_dict = GeometryCollection(return_geo)
            else:
                return_dict = dict_data
        elif (
            isinstance(dict_data, list)
            or isinstance(dict_data, tuple)
            or isinstance(dict_data, set)
        ):
            len_data = 0
            data_list = []

            for data in dict_data:
                data_list.append(
                    item_data := json2geojson(data, dict_params)
                )

                if len_data < (
                    new_len_data := len(item_data)
                ):
                    len_data = new_len_data

            if len_data:
                return_dict = FeatureCollection(data_list)

    return return_dict
