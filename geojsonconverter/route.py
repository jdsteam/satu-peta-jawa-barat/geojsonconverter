#!/usr/bin/env python3

# MIT License

# Copyright (c) 2019 Pipin Fitriadi

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from json import decoder, loads
from urllib.parse import urlparse

from flask import abort, Blueprint, current_app, redirect, request
from flask_cors import cross_origin
import requests as req

from .library import json2geojson

app = Blueprint('route', __name__, url_prefix='/')


@app.route('/', methods=['POST'])
@cross_origin(supports_credentials=True)
def root():
    status_code = 200
    headers = 'application/json'

    if (
        (data := request.get_json(silent=True))
        and (source := data.get('source'))
    ):
        try:
            url_valid = all([
                (url_valid := urlparse(source)).scheme,
                url_valid.netloc
            ])
        except AttributeError:
            url_valid = False

        if url_valid:
            try:
                method = data.get('method').upper()
            except AttributeError:
                method = 'GET'

            is_method_head = False

            if method == 'HEAD':
                method = req.head
                is_method_head = True
            elif method == 'POST':
                method = req.post
            elif method == 'PUT':
                method = req.put
            elif method == 'DELETE':
                method = req.delete
            elif method == 'OPTIONS':
                method = req.options
            elif method == 'PATCH':
                method = req.patch
            else:
                method = req.get

            method_params = {
                'url': source,
                **{
                    key: value
                    for key, value in {
                        'params': request.args,
                        'json': data.get('body'),
                        'headers': data.get('headers')
                    }.items()
                    if value
                }
            }

            if (
                not is_method_head
                and (
                    content_length := req.head(
                        **method_params
                    ).headers.get('Content-Length')
                )
                and int(content_length) > current_app.config.get(
                    'MAX_CONTENT_LENGTH'
                )
            ):
                abort(413)

            response = method(**method_params)
            status_code = response.status_code
            source = response.text

            if (
                headers := response.headers['Content-Type']
            ) == 'application/json':
                source = loads(source)

        json2geojson_params = data.get('json2geojson')
    else:
        try:
            source = loads(
                request.form.get('source')
                or (
                    data.read()
                    if (data := request.files.get('source'))
                    else data
                )
            )
        except (
            decoder.JSONDecodeError,
            TypeError,
            UnicodeDecodeError
        ):
            source = None

        try:
            json2geojson_params = loads(
                request.form.get('json2geojson')
                or (
                    data.read()
                    if (data := request.files.get('json2geojson'))
                    else data
                )
            )
        except TypeError:
            json2geojson_params = None

    if (
        json2geojson_params
        and source
        and status_code == 200
        and headers == 'application/json'
    ):
        if (
            response := json2geojson(
                source,
                json2geojson_params
            )
        ):
            source = (
                response.popitem()[1]
                if len(response) == 1
                else response
            )
        else:
            source = None

    if not source:
        abort(422)

    return (
        {
            'data': source,
            'error': None
        },
        status_code,
        {
            'Content-Type': headers
        }
    )


@app.route('/license/')
@cross_origin(supports_credentials=True)
def license():
    return (
        open('LICENSE', 'r').read(),
        200,
        {
            'Content-Type': 'text/plain'
        }
    )
