flake8

# How to address 'OSError: libc not found' raised on Gunicorn exec of Flask app inside Alpine docker container:
# https://stackoverflow.com/questions/58786695/how-to-address-oserror-libc-not-found-raised-on-gunicorn-exec-of-flask-app-in
gunicorn==19.9.0

flask
flask-cors
flask-swagger-ui
requests
geojson
